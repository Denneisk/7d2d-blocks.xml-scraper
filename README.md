Generates a wikitable formatted table out of blocks.xml from 7 Days To Die.

Build instructions:

- Clone the repository.

- Run `dotnet publish` in the repository directory.