﻿using System;
using System.Xml;
using System.IO;
using System.Collections;

namespace xmlscraper
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {Console.WriteLine("Error: file input expected");Environment.Exit(1);}

            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = true;

            try {doc.Load(args[0]);}
            catch(System.IO.FileNotFoundException)
            {Console.WriteLine($"Error: file not found ({args[0]}"); Environment.Exit(3);}

            XmlNodeList nodes;
            XmlNode root = doc.DocumentElement;

            nodes = root.SelectNodes("//block/property[@name='HeatMapStrength']/..");

            ArrayList arr = new ArrayList(8);
            foreach(XmlNode node in nodes)
            {
                double Frequency;
                {
                    var thing = node.SelectSingleNode("./property[@name='HeatMapFrequency']");
                    if(thing != null)
                        Frequency = Convert.ToDouble(thing.Attributes.GetNamedItem("value").Value);
                    else Frequency = 0;
                }
                HeatBlock block = new HeatBlock{
                    Name = node.Attributes.GetNamedItem("name").Value,
                    Strength = Convert.ToDouble(node.SelectSingleNode("./property[@name='HeatMapStrength']").Attributes.GetNamedItem("value").Value),
                    Time = Convert.ToDouble(node.SelectSingleNode("./property[@name='HeatMapTime']").Attributes.GetNamedItem("value").Value),
                    Frequency = Frequency
                };
                arr.Add(block);
            }
            
            StreamWriter output = new StreamWriter("out.txt");
            output.WriteLine("{| class=\"wikitable\""+
                           "\n|+ Block Heat"+
                           "\n|-"+
                           "\n! Block"+
                           "\n! Strength"+
                           "\n! Time"+
                           "\n! Frequency"+
                           "\n|-");

            foreach(HeatBlock block in arr)
            {
                output.WriteLine($"|-\n! {block.Name}\n| {block.Strength} || {block.Time} || {(block.Frequency!=0?Convert.ToString(block.Frequency):"default")}");
            }

            output.WriteLine("|}");
            output.Close();
            Console.WriteLine($"Finished with {arr.Count} elements.");

        }
    }

    public class HeatBlock
    {
        public double Strength;
        public double Time;
        public string Name;
        public double Frequency;
    }
}
